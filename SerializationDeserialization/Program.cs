﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace SerializationDeserialization
{
    class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student() { FirstName = "Trisna", LastName = "Tanjung" };
            string filePath = "data.save";
            DataSerializer dataSerializer = new DataSerializer();
            Student s = null;

            dataSerializer.BinarySerialize(student, filePath);

            s = dataSerializer.BinaryDeserialize(filePath) as Student;

            Console.WriteLine(s.FirstName);
            Console.WriteLine(s.LastName);

            Console.ReadLine();
        }

        [Serializable]
        public class Student
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
        }

        class DataSerializer
        {
            public void BinarySerialize(object data, string filePath)
            {
                FileStream fileStream;
                BinaryFormatter bf = new BinaryFormatter();
                if (File.Exists(filePath)) File.Delete(filePath);
                fileStream = File.Create(filePath);
                bf.Serialize(fileStream, data);
                fileStream.Close();
            }

            public object BinaryDeserialize(string filePath)
            {
                object obj = null;

                FileStream fileStream;
                BinaryFormatter bf = new BinaryFormatter();
                if (File.Exists(filePath))
                {
                    fileStream = File.OpenRead(filePath);
                    obj = bf.Deserialize(fileStream);
                    fileStream.Close();

                }

                return obj;
                



            }
            
            

          
        }


    }
}
